const puppeteer = require('puppeteer');

const criUrl = 'https://crisrv2.intranet.epfl.ch/fmi/webd/Vente_Materiel';

async function getMainTextArea(page) {
  return (await page.$$("div.fm-textarea")).pop()
}

async function awaitTruthy(page, f) {
  for(let i = 0; i < 50; i++) {
    let retval
    try {
      retval = await f(page)
      if (retval) {
        return retval
      }
    } catch (e) {
    }
    await page.waitFor(100)
  }
  throw new Error("Timeout waiting for function to return truthy: " + f)
}

async function main () {
  const browser = await puppeteer.launch({headless: ! process.env.HEADFUL });
  const page = await browser.newPage();
  await page.goto(criUrl)

  let mainTextArea = await awaitTruthy(page, getMainTextArea)
  console.log(await mainTextArea.$$eval('div', (elements) => elements.map (e => e.innerHTML)))

  await browser.close();
}

main().catch(console.error);
